# Prismeai Dsul Loadtest

This small tool allow you to load test both Prismeai pages & workspaces with detailed performance measures retrieved using [Artillery](https://www.artillery.io/) and artillery plugins tailored to seamlessly integrate with Prismeai platform.  

**Disable or increase runtime rate limits to avoid throttling during performance tests :**  

```
RATE_LIMIT_DISABLED=true
```

See https://docs.eda.prisme.ai/en/architecture/technical/runtime/rateLimits/  

## Getting started

When cloning this repository  for the first time, dependencies must be installed with  
```bash
npm install
```


`templates/` directory include examples of artillery load test files.  
Execute the following command to launch any of them :  
```bash
npm start templates/basicEmits.yml
```

When testing with [websockets transport](#websocket-transport) (like this `basicEmits.yml` template), anonymous sessions will be automatically generated for each new [VU (Virtual User)](https://www.artillery.io/docs/get-started/core-concepts#virtual-users) so each scenario execution will be able to benefit from their own separated prismeai runtime contexts.  

The following user variables are defined by `artillery-prismeai-plugins` :  

* **userId** : Curent anonymous userId  
* **sessionId** : Current anonymous sessionId   
* **token** : Current anonymous token, can be used for HTTP requests  


## Artillery configuration  

### Websocket transport
In order to load test a workspace using events & websocket transport (i.e which is the Prismeai standardized way of building production ready applications), the `config.target` must point to the workspace events API :  
```yaml
config:
  target: "https://api.studio.prisme.ai/v2/workspaces/<INSERT HERE YOUR WORKSPACE ID>/events"
```

`artillery-engine-socketio-v3` and `artillery-prismeai-plugins` must also be enabled :  
```yaml
config:
  target: "https://api.studio.prisme.ai/v2/workspaces/<INSERT HERE YOUR WORKSPACE ID>/events"
  socketio: {}
  phases:
    - duration: 2
      arrivalRate: 1
  plugins:
    prismeai: {}
  engines:
    socketio-v3: {}
```

Finally, after configuring [phases evolution](https://www.artillery.io/docs/get-started/core-concepts#load-phases), last step is to write the [scenario](https://www.artillery.io/docs/reference/test-script#scenarios-section).  
`artillery-prismeai-plugin` provides a `wait` instruction to used after `artillery-engine-socketio-v3` : this new instruction allows you to pause your test until a specific event type is received, or until `timeout` ms has elapsed without receiving said event (defaults to 2000 ms), in which case the test fails.  

Example scenario :  
```yaml
scenarios:
  - name: "Connect and send a bunch of messages"
    engine: socketio-v3
    flow:
      - emit:
          - event
          - type: init
            payload:
              foo: bar
      - wait:
          event: updateText
          filters:
            source.sessionId: '{{ sessionId }}'
            source.socketId: '{{ socketId }}'
          timeout: 3000
```

Above `wait.filters` lets you filter expected event with current `sessionId` and `socketId` to make sure we receive the response event emitted inside our current test session. Any other variable defined from an [Artillery processor function](https://www.artillery.io/docs/reference/test-script#processor---load-custom-code) or some other native instruction will be also available to `{{ yourVariableInterpolation }}`.  


## Interpreting results

Here is an example of Artillery final output using `templates/basicEmits.yml` :  

```
engine.socketio.emit: .......................................................... 2
engine.socketio.emit_rate: ..................................................... 2/sec
engine.socketio.rcv: ........................................................... 8
engine.socketio.rcv_rate: ...................................................... 7/sec
prismeai.automations_execution: ................................................ 2/sec
prismeai.automations_execution.init:
  min: ......................................................................... 1
  max: ......................................................................... 2
  mean: ........................................................................ 1.5
  median: ...................................................................... 1
  p95: ......................................................................... 1
  p99: ......................................................................... 1
prismeai.emits_ack:
  min: ......................................................................... 31
  max: ......................................................................... 48
  mean: ........................................................................ 39.5
  median: ...................................................................... 30.9
  p95: ......................................................................... 30.9
  p99: ......................................................................... 30.9
prismeai.waits.updateText:
  min: ......................................................................... 0
  max: ......................................................................... 1
  mean: ........................................................................ 0.5
  median: ...................................................................... 0
  p95: ......................................................................... 0
  p99: ......................................................................... 0
vusers.completed: .............................................................. 2
vusers.created: ................................................................ 2
vusers.created_by_name.Connect and send a bunch of messages: ................... 2
vusers.failed: ................................................................. 0
vusers.session_length:
  min: ......................................................................... 604.1
  max: ......................................................................... 704.9
  mean: ........................................................................ 654.5
  median: ...................................................................... 608
  p95: ......................................................................... 608
  p99: ......................................................................... 608
```

### Metrics explanation

* **engine.socketio.emit** : Total number of events emitted during the test  
* **engine.socketio.emit_rate** : Average rate of emitted events  
* **engine.socketio.rcv** : Total number of events received durig the test. Websocket clients only receive events they are filtering on **and** they are allowed to read. Total number of events processed by the platform is very likely higher than that metric, which only represents how many were receive by our test sockets  
* **engine.socketio.rcv_rate** : Average rate of received events   
* **prismeai.automations_execution** : Automations execution rate. See how to enable [automations metrics](#automations-metrics)
* **prismeai.automations_execution.AUTOMATION_SLUG** : Execution time histogram, for each distinct automation slug
* **prismeai.emits_ack** :  Histogram descriging the time taken to receive our own emitted events, acknowledging they have been sent to the platform  
* **prismeai.waits.EVENT_TYPE** : Histogram describing the time taken to receive each distinct event type  
* **vusers.created** : Total number of executed VUs
* **vusers.completed** : Number of VU which succesfully completed the scenarios  
* **vusers.failed** : Number of failed VU
* **errors.WaitTimeout.EVENT_TYPE** : Number of waits instructions (& entire VUs, as a wait timeout make his VU immediately exit) which timeouted waiting for given event type  
* **vusers.session_length** : Histogram describing the time taken to complete the entire scenario

#### Automations metrics

In order to track automation metric, the following [security rule](https://docs.eda.prisme.ai/en/workspaces/security/#rule-structure) must be appended to your workspace security manifest :  

```yaml
    - action: read
      subject: events
      conditions:
        source.sessionId: '{{session.id}}'
      reason: Anyone can read any events from its own session (source=customRoles:migrated)
```

A similar rule likely already exists in your manifest, from which you only have to remove the `source.serviceTopic: topic:runtime:emit` condition
