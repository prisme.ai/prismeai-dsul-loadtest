async function randomUserAgent(ctx, events) {
  ctx.vars.userAgent = "artillery-" + Math.round(Math.random() * 100000);
}

module.exports = {
  randomUserAgent,
};
