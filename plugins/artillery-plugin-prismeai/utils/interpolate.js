const { get } = require("lodash");

function interpolateString(str, values = {}) {
  return str.replace(/\{\{([^}]+)\}\}/g, (_, m) => {
    return get(values, m.trim()) || "";
  });
}

module.exports = function interpolate(into, values) {
  if (typeof into === "string") {
    return interpolateString(into, values);
  }
  if (typeof into === "number" || typeof into === "boolean") {
    return into;
  }
  const isArray = Array.isArray(into);
  const newObject = isArray ? [...into] : { ...into };
  for (const key of Object.keys(newObject)) {
    const value = newObject[key];
    newObject[key] = interpolate(value, values);
  }
  return newObject;
};
