const extractObjectsByPath = require("./extractObjectsByPath.js");

module.exports = function jsonPathMatches(jsonPaths, object) {
  if (!jsonPaths) {
    return true;
  }

  return Object.entries(jsonPaths)
    .map(([k, expected]) => {
      const found = extractObjectsByPath(object, k);
      if (!expected) {
        return !found;
      }
      if (
        typeof found !== "string" ||
        typeof expected !== "string" ||
        (expected[expected.length - 1] !== "*" && expected[0] !== "*")
      ) {
        return found === expected;
      }
      // Only support beginning OR ending wildcard for the moment
      return expected[0] === "*"
        ? found.endsWith(expected.slice(1))
        : found.startsWith(expected.slice(0, -1));
    })
    .every(Boolean);
};
