const EventEmitter = require("node:events");
const jsonPathMatches = require("./utils/jsonPathMatches.js");
const interpolate = require("./utils/interpolate.js");
const debug = require("debug")("prismeai");

const api = require("./sdk/index.js");

let __receivedEvents = {}; // Map VU _uid id to event ids to events
let __parameters = {}; // Map functionId (i.e 'wait') to the list of params in the exact order of wait instructions in yaml file
let __parametersByVu = {}; // Map VU _uid to functionIds to the list of params

function getNextParams(vuId, funcId) {
  if (!__parametersByVu[vuId]) {
    __parametersByVu[vuId] = {};
  }
  if (!__parametersByVu[vuId][funcId]) {
    __parametersByVu[vuId][funcId] = [...(__parameters[funcId] || [])];
  }

  return __parametersByVu[vuId][funcId].shift();
}

function getEventsStore(ctx, eventType) {
  if (!(ctx._uid in __receivedEvents)) {
    const emitter = new EventEmitter();
    emitter.setMaxListeners(2000);

    const alreadyProcessed = new Set();
    __receivedEvents[ctx._uid] = {
      alreadyProcessed,
      events: {},
      emitter,
      emit: (eventType, event) => {
        if (!(eventType in __receivedEvents[ctx._uid].events)) {
          __receivedEvents[ctx._uid].events[eventType] = [];
        }
        __receivedEvents[ctx._uid].events[eventType].push(event);
        __receivedEvents[ctx._uid].emitter.emit(eventType, event);
      },
      waitFor: async (eventType, filters, timeout) => {
        // Check if the event has already been received
        if (
          eventType in __receivedEvents[ctx._uid].events &&
          __receivedEvents[ctx._uid].events[eventType].length
        ) {
          const matchingEvent = __receivedEvents[ctx._uid].events[
            eventType
          ].find(
            (event) =>
              event &&
              !alreadyProcessed.has(event?.id) &&
              jsonPathMatches(filters, event)
          );
          if (matchingEvent) {
            alreadyProcessed.add(matchingEvent.id);

            // debug(`Wait : Returning already received ${eventType}`);
            return matchingEvent;
          }
        }

        // debug(
        //   `Wait : ${eventType} not found in already received events, waiting ${
        //     timeout || 2000
        //   }ms ...`
        // );
        // Wait for this event to be received
        return new Promise((resolve, reject) => {
          const timer = setTimeout(
            () => reject({ error: "Timeout" }),
            timeout || 2000
          );

          const handler = function handler(event) {
            if (
              jsonPathMatches(filters, event) &&
              !alreadyProcessed.has(event.id)
            ) {
              alreadyProcessed.add(event.id);
              clearTimeout(timer);
              resolve(event);
            } else {
              // debug(
              //   `Received event ${eventType} ${JSON.stringify(
              //     event
              //   )} mismatch with filters ${JSON.stringify(filters)}`
              // );
              emitter.once(eventType, handler);
            }
          };
          emitter.once(eventType, handler);
        });
      },
    };
  }
  return __receivedEvents[ctx._uid];
}

// Exposed Artillery functions

async function wait(ctx, events, done) {
  const parameters = interpolate(getNextParams(ctx._uid, "wait"), ctx.vars);
  const store = getEventsStore(ctx, parameters.event);
  // If the event has been received 0 or 1ms earlier, it would be missed without this
  // await new Promise((resolve) => setImmediate(resolve));
  await new Promise((resolve) => setTimeout(resolve, 100));
  let t0 = Date.now();
  try {
    const event = await store.waitFor(
      parameters.event,
      parameters.filters,
      parameters.timeout
    );
    let t1 = Date.now();
    events.emit("histogram", "prismeai.waits." + event.type, t1 - t0);
    done();
  } catch (err) {
    const lastEmitType = ctx.vars?.lastEvent?.type || ctx.lastEmit;
    const lastEmitDate = ctx.emittedEvents[lastEmitType].slice(-1)[0];
    console.error(
      `Timed out at ${Date.now()} after ${Date.now() - t0}ms waiting for '${
        parameters.event
      }' event after emitting '${lastEmitType}' (ID: ${
        ctx?.vars?.lastEvent?.id
      }, correlationId: ${ctx.vars?.lastEvent?.source?.correlationId}, VU: ${
        ctx._uid
      }) at ${lastEmitDate}`
    );
    console.error(
      `Filters : ${JSON.stringify(
        parameters.filters
      )}, connected : ${JSON.stringify(ctx.connected)}`
    );
    done(new Error("WaitTimeout." + parameters.event));
  }
}

// Internal functions
function __onSocketioMessage(ctx, channel, data, ee, socket) {
  if (channel === "error") {
    console.error("Received error ", JSON.stringify(data, null, 2));
    ee.emit("error", new Error(data?.error));
    return;
  }

  const store = getEventsStore(ctx, data.type);
  store.emit(data.type, data);
  if (
    data.source?.serviceTopic === "topic:runtime:emit" &&
    data?.source?.userId === ctx.vars.userId
  ) {
    ctx.vars.lastEvent = data;
    const emittedAt = ctx.emittedEvents?.[data.type]
      ? ctx.emittedEvents[data.type].shift()
      : undefined;
    if (emittedAt) {
      ee.emit("histogram", "prismeai.emits_ack", Date.now() - emittedAt);
    }
  }

  if (data.type === "error") {
    ee.emit("counter", "prismeai.events.error", 1);
    ee.emit("rate", "prismeai.events.error_rate");
  } else if (data.type === "runtime.automations.executed") {
    ee.emit(
      "histogram",
      "prismeai.automations_execution." + data.payload.slug,
      data.payload.duration
    );
    ee.emit("rate", "prismeai.automations_execution");
    if (data.payload?.throttled) {
      console.warn(
        "Runtime automations are currently rate limited. Raise or disable rate limits to avoid throttling during performance tests. "
      );
    }
  } else if (data?.payload?.userTopics) {
    socket.emit("filters", {
      payloadQuery: [
        {
          "source.sessionId": ctx.vars.sessionId,
          "target.userTopic": "",
        },
        {
          "target.userTopic": data?.payload?.userTopics,
        },
      ],
    });
  }
}

function __addFunctionParameters(func, parameters) {
  if (!__parameters[func]) {
    __parameters[func] = [];
  }
  __parameters[func].push(parameters);
}

module.exports = {
  wait,
  prismeaiAuth: async function auth(ctx, event, done) {
    try {
      (await api(ctx)).user;
      done();
    } catch (err) {
      done(err);
    }
  },
  __onSocketioMessage,
  __onSocketioEmit: (ctx, type, event) => {
    debug(`${ctx._uid} emitting ${type} ${event.type}`);
    if (type !== "event" || !event?.type) {
      return;
    }
    if (!ctx.emittedEvents) {
      ctx.emittedEvents = {};
    }
    if (!ctx.emittedEvents[event.type]) {
      ctx.emittedEvents[event.type] = [];
    }
    ctx.emittedEvents[event.type].push(Date.now());
    ctx.lastEmit = event.type;
  },
  __addFunctionParameters,
  __onSocketioConnection: async function __onSocketioConnection(ctx) {
    if (!ctx.vars.token) {
      (await api(ctx)).user;
    }
    return {
      extraHeaders: {
        Authorization: "Bearer " + ctx.vars.token,
        "User-Agent": "Prismai Artillery VU " + ctx._uid,
      },
      auth: {
        filters: {
          payloadQuery: [
            {
              "source.sessionId": ctx.vars.sessionId,
              "target.userTopic": "",
            },
          ],
        },
      },
    };
  },
  __onSocketioConnected: async function __onSocketioConnected(ctx, socket) {
    ctx.connected = {
      socketId: socket.id,
      userId: ctx.vars.userId,
      at: Date.now(),
    };
    debug(
      `${ctx._uid} connected with socketId: ${socket.id} and sessionId: ${ctx.vars.sessionId}`
    );
  },
};
