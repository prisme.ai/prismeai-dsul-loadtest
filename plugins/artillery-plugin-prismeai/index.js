/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

"use strict";

const debug = require("debug")("plugin:prismeai");
const socketioHooks = require("./socketioHooks");
const api = require("./sdk/index.js");

module.exports.Plugin = ArtilleryPrismeaiPlugin;

function ArtilleryPrismeaiPlugin(script, events) {
  if (typeof process.env.LOCAL_WORKER_ID === "undefined") {
    debug("Not running in a worker, exiting");
    return;
  }

  // This is the entirety of the test script - config and
  // scenarios
  this.script = script;
  // This is an EventEmitter, we can subscribe to:
  // 'stats' - fired when a new batch of metrics is available
  // 'done' - fired when all VUs are done
  // We can also use this EventEmitter to emit custom
  // metrics:
  // https://artillery.io/docs/guides/guides/extending.html#Tracking-custom-metrics
  this.events = events;

  // We can read our plugin's configuration:
  // const pluginConfig = script.config.plugins["prismeai"];

  // Create processor object if needed to hold our custom function:
  script.config.processor = script.config.processor || {};

  // Load our custom hooks
  Object.keys(socketioHooks).forEach((func) => {
    script.config.processor[func] = socketioHooks[func];
  });

  // Parse scenarios & inject our custom instructions
  for (let [sc_id, scenario] of Object.entries(script.scenarios)) {
    for (let [fl_id, flow] of Object.entries(scenario.flow)) {
      for (let [ac_id, action] of Object.entries(flow)) {
        if (ac_id in socketioHooks) {
          script.scenarios[sc_id].flow[fl_id] = {
            function: ac_id,
          };
          socketioHooks.__addFunctionParameters(ac_id, action);
        }
      }
    }
  }
  return this;
}

// Artillery will call this before it exits to give plugins
// a chance to clean up, e.g. by flushing any in-flight data,
// writing something to disk etc.
ArtilleryPrismeaiPlugin.prototype.cleanup = function (done) {
  done(null);
};
