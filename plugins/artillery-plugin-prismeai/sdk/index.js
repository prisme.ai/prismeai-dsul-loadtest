const fetch = require("node-fetch");
const fs = require("fs");

class PrismeaiSDK {
  apiHost;
  user;

  constructor(apiHost, user = null) {
    this.apiHost = apiHost;
    this.user = user || null;
  }

  async fetch(path, body, opts) {
    const { method = "POST" } = opts || {};
    const url = new URL(path, this.apiHost).toString();
    const headers = {
      "Content-Type": "application/json",
      ...opts?.headers,
    };
    if (this.user?.token) {
      headers["Authorization"] = `Bearer ${this.user?.token}`;
    }
    const req = await fetch(url, {
      method,
      headers,
      body: body ? JSON.stringify(body) : undefined,
    });
    const result = req.headers
      .get("content-type", "")
      .includes("application/json")
      ? await req.json()
      : await req.text();
    if (typeof result === "string") {
      throw result;
    }
    if (req.status > 399) {
      throw result;
    }
    return result;
  }

  async anonymousLogin(retries = 2) {
    try {
      this.user = await this.fetch("v2/login/anonymous", {
        expiresAfter: 3600, // Expires our test sessions after 1h
      });
      return this.user;
    } catch (err) {
      if (retries > 0) {
        console.error("Failed anonymous authentication : ", err);
        return this.anonymousLogin(retries - 1);
      }
      throw err;
    }
  }
}

let sdks = {}; // Map 1 new anonymous authenticated API per VU

module.exports = async function api(ctx) {
  if (!sdks[ctx._uid]) {
    let target = new URL(ctx.vars.target);
    const prismeaiUrl = target.origin + "/v2";
    sdks[ctx._uid] = new PrismeaiSDK(prismeaiUrl);
    const user = await sdks[ctx._uid].anonymousLogin();
    ctx.vars.userId = user.id;
    ctx.vars.sessionId = user.sessionId;
    ctx.vars.token = user.token;
  }
  return sdks[ctx._uid];
};
